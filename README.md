# Metadata dictionary #

This is a DHIS2 learning app that enables the user to to test their knowledge of DHIS2. This app has 2 module.

* Course Taker- used to take courses which were assigned to some user. This is a dashboard application

* Course Writer- used by administrators to set courses

# Installation #
* Course taker
Zip (compress) the app folder. Go to DHIS2 | App Management | upload the zipped file | Go to the users Dashboard | Search for the Smart-learn-course-taker | click add | Application will appear on the dashboard | click on the dashboard to use it

* Course Writer
Zip (compress) the app folder. Go to DHIS2 | App Management | upload the zipped file


### Authors ###
* Yamikani Phiri
* Lawrence Byson